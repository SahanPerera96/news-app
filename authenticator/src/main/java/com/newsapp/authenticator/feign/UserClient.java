package com.newsapp.authenticator.feign;

import com.newsapp.authenticator.model.RegisterRequest;
import com.newsapp.authenticator.model.SaveResponseDTO;
import com.newsapp.authenticator.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;

@FeignClient(name = "profile", url = "${application.config.user-profile}")
public interface UserClient {


    @GetMapping("email/{email}")
    Optional<User> findByEmail(@PathVariable("email") String email);

    @PostMapping("register")
     ResponseEntity<SaveResponseDTO> register(@RequestBody RegisterRequest request);
}
