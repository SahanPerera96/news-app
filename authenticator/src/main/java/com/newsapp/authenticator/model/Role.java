package com.newsapp.authenticator.model;

public enum Role {

    USER,
    ADMIN
}
