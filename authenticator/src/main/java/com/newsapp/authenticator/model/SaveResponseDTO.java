package com.newsapp.authenticator.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SaveResponseDTO {
    private boolean status;
    private String message;
    private User payload;
}