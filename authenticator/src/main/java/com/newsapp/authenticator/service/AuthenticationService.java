package com.newsapp.authenticator.service;

import com.newsapp.authenticator.config.JwtService;
import com.newsapp.authenticator.feign.UserClient;
import com.newsapp.authenticator.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final UserClient userClient;

    public AuthenticationResponse register(RegisterRequest request) {
        var response = AuthenticationResponse.builder().status(false).build();
        try {
            ResponseEntity<SaveResponseDTO> savedEntity = userClient.register(request);
            if (savedEntity.getStatusCode().equals(HttpStatus.OK)) {
                var jwtToken = jwtService.generateToken(Objects.requireNonNull(savedEntity.getBody()).getPayload());
                response.setStatus(true);
                response.setToken(jwtToken);
            } else {
                response.setMessage("Failed to save user");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setMessage("Failed to connect to server to save user");
        }
        return response;

    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        var response = AuthenticationResponse.builder().status(false).build();
        try {
            // authenticate user credentials
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getEmail(),
                            request.getPassword()
                    )
            );
            // generate JWT only of authentication is a success
            if (authentication.isAuthenticated()) {
                var jwtToken = jwtService.generateToken((UserDetails) authentication.getPrincipal());
                response.setStatus(true);
                response.setToken(jwtToken);
            } else {
                response.setMessage("Failed to verify credentials");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setMessage("Failed to connect to server to verify credentials");
        }
        return response;
    }
}
