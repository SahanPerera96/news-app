package com.newsapp.user.profile.service;

import com.newsapp.user.profile.model.RegisterRequest;
import com.newsapp.user.profile.model.Role;
import com.newsapp.user.profile.model.SaveResponseDTO;
import com.newsapp.user.profile.model.User;
import com.newsapp.user.profile.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public SaveResponseDTO saveUser(RegisterRequest request) {
        var user = User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.USER)
                .build();

        var response = SaveResponseDTO.builder().status(false).build();
        try {
            // Check if email already exists
            if (userRepository.findByEmail(user.getEmail()).isPresent()) {
                logger.error("Email '{}' already exists", user.getEmail());
                response.setMessage("Email already exists");

            } else {
                logger.info("Saving user with email '{}'", user.getEmail());
                User savedUser = userRepository.save(user);
                response.setStatus(true);
                response.setPayload(savedUser);
            }

        } catch (Exception e) {
            e.printStackTrace();
            response.setMessage("Failed to save user to DB");
        }

        return response;
    }

    @Override
    public Optional<User> findByEmail(String email) {
        try {
            return userRepository.findByEmail(email);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
