package com.newsapp.user.profile.model;

public enum Role {

    USER,
    ADMIN
}
