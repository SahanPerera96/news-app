//package com.newsapp.user.profile.service;
//
//import com.newsapp.user.profile.config.JwtService;
//import com.newsapp.user.profile.model.*;
//import com.newsapp.user.profile.repository.UserRepository;
//import lombok.RequiredArgsConstructor;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.http.HttpStatus;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//import java.util.Optional;
//
//@Service
//@RequiredArgsConstructor
//public class UserProfileService {
//    private static final Logger logger = LoggerFactory.getLogger(UserProfileService.class);
//
//    private final UserRepository userRepository;
//    private final PasswordEncoder passwordEncoder;
//    private final JwtService jwtService;
//
//    public ResponseBody register(RegisterRequest request) throws Exception {
//        var user = User.builder()
//                .firstName(request.getFirstName())
//                .lastName(request.getLastName())
//                .email(request.getEmail())
//                .password(passwordEncoder.encode(request.getPassword()))
//                .role(Role.USER)
//                .build();
//
//        // Check if email already exists
//        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
//            logger.error("Email '{}' already exists", user.getEmail());
//                throw new Exception("Email already exists");
//        }
//        logger.info("Saving user with email '{}'", user.getEmail());
//        userRepository.save(user);
//        logger.info("Generating token for user '{}", user.getEmail());
//        // generating jwt token once user is saved
//        var jwtToken = jwtService.generateToken(user);
//        return ResponseBody.builder()
//                .body(AuthenticationResponse.builder()
//                        .token(jwtToken).build())
//                .statusCode(HttpStatus.CREATED.value())
//                .status(Status.SUCCESS)
//                .build();
//    }
//
//    public ResponseBody fetchAllUsers() throws Exception {
//        try{
//            return ResponseBody.builder()
//                    .body(userRepository.findAll())
//                    .statusCode(HttpStatus.OK.value())
//                    .status(Status.SUCCESS)
//                    .build();
//        }catch (Exception ex){
//            throw new Exception(ex);
//        }
//    }
//
//    public Optional<User> findByEmail(String email) throws Exception{
//     return userRepository.findByEmail(email);
////      if(optionalUser.isPresent()){
////          return optionalUser.get();
////      }
////      return new User();
//    }
//}
