//package com.newsapp.user.profile.security;
//
//import com.newsapp.user.profile.config.JwtAuthenticatorFilter;
//import com.newsapp.user.profile.exception.CustomAccessDeniedHandler;
//import lombok.RequiredArgsConstructor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.web.SecurityFilterChain;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
//@Configuration
//@RequiredArgsConstructor
//@EnableMethodSecurity
//public class SecurityConfiguration {
//
//    private final JwtAuthenticatorFilter jwtAuthenticatorFilter;
//    private final AuthenticationProvider authenticationProvider;
//    private final CustomAccessDeniedHandler customAccessDeniedHandler;
//    @Bean
//    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{
//         http.csrf().disable()
//                .authorizeHttpRequests().requestMatchers("/api/v1/user/register")
//                .permitAll()
//                .anyRequest().authenticated()
//                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                .and().authenticationProvider(authenticationProvider)
//                .addFilterBefore(jwtAuthenticatorFilter, UsernamePasswordAuthenticationFilter.class)
//                 .exceptionHandling()
//                 .accessDeniedHandler(customAccessDeniedHandler);;
//
//        return http.build();
//    }
//}
