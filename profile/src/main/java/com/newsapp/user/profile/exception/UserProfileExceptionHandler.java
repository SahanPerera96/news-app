//package com.newsapp.user.profile.exception;
//
//import com.newsapp.user.profile.model.ResponseBody;
//import com.newsapp.user.profile.model.Status;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseStatus;
//
//import java.nio.file.AccessDeniedException;
//import java.util.Arrays;
//import java.util.Optional;
//
//@ControllerAdvice
//public class UserProfileExceptionHandler {
//    private static final Logger logger = LoggerFactory.getLogger(UserProfileExceptionHandler.class);
//
//    @ExceptionHandler
//    public ResponseEntity<ResponseBody> handleGlobalException(Exception ex) {
//        logger.error(generateErrorDetails(ex));
//        ResponseBody responseBody = ResponseBody.builder()
//                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
//                .status(Status.ERROR)
//                .message("Some issue in the service")
//                .build();
//        return new ResponseEntity<>(responseBody, HttpStatus.INTERNAL_SERVER_ERROR);
//    }
//    @ExceptionHandler(AccessDeniedException.class)
//    @ResponseStatus(HttpStatus.FORBIDDEN)
//    public ResponseEntity<ResponseBody> handleAccessDeniedException(AccessDeniedException ex) {
//        logger.error(generateErrorDetails(ex));
//        ResponseBody responseBody = ResponseBody.builder()
//                .statusCode(HttpStatus.FORBIDDEN.value())
//                .status(Status.ERROR)
//                .message("Access Denied: You do not have the necessary permissions to access this resource.")
//                .build();
//        return new ResponseEntity<>(responseBody, HttpStatus.FORBIDDEN);
//    }
//    /**
//     *
//     * @param ex - thrown exception details
//     *           Identifies the stack trace to get the related error points in the application by reading the class path
//     *           The first stack trace denotes the last location point where the error was thrown from
//     *
//     * @return - formatted last error occurrence point
//     */
//    private String generateErrorDetails(Exception ex){
//
//        Optional<StackTraceElement> stackTraceElementOptional = Arrays.asList(ex.getStackTrace()).stream()
//                .filter(o -> o.getClassName().contains("com.newsapp.user.profile")).findFirst();
//        if(stackTraceElementOptional.isPresent()){
//            StringBuilder sb = stringifyErrorLocationDetails(stackTraceElementOptional);
//            sb.append("\n Error message: ");
//            sb.append(ex.getMessage());
//            return sb.toString();
//        }
//        return "Unable to extract error details";
//    }
//    private StringBuilder stringifyErrorLocationDetails(Optional<StackTraceElement> stackTraceElementOptional) {
//        StackTraceElement exceptionLocation = stackTraceElementOptional.get();
//        String className = exceptionLocation.getClassName();
//        String methodName = exceptionLocation.getMethodName();
//        int lineNumber = exceptionLocation.getLineNumber();
//        StringBuilder sb = new StringBuilder();
//        sb.append("Exception occurred in class: ");
//        sb.append(className);
//        sb.append(", Method: ");
//        sb.append(methodName);
//        sb.append(", Line number: ");
//        sb.append(lineNumber);
//        return sb;
//    }
//}
