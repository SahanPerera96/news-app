//package com.newsapp.user.profile.controller;
//
//import lombok.RequiredArgsConstructor;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("/api/v1/demo")
//@RequiredArgsConstructor
//public class DemoController {
//
//    @GetMapping
//    public ResponseEntity<String> sayHello() {
//        return ResponseEntity.ok("Hello from secured endpoint");
//    }
//    @GetMapping("/admin")
//    @PreAuthorize("hasAuthority('ADMIN')")
//    public ResponseEntity<String> getValue(){
//        return ResponseEntity.ok("SecuredBody for Admin");
//    }
//    @GetMapping("/user")
//    @PreAuthorize("hasAuthority('USER')")
//    public ResponseEntity<String> getUser(){
//        return ResponseEntity.ok("SecuredBody for Users");
//    }
//}
