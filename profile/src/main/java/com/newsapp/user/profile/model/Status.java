package com.newsapp.user.profile.model;

public enum Status {
    SUCCESS,
    FAIL,
    UNAUTHORISED,
    ERROR
}
