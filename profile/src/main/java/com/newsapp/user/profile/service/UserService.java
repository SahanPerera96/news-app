package com.newsapp.user.profile.service;

import com.newsapp.user.profile.model.RegisterRequest;
import com.newsapp.user.profile.model.SaveResponseDTO;
import com.newsapp.user.profile.model.User;

import java.util.Optional;

public interface UserService {

    SaveResponseDTO saveUser(RegisterRequest request);
    Optional<User> findByEmail(String email);
}
