//package com.newsapp.user.profile.config;
//
//import jakarta.servlet.FilterChain;
//import jakarta.servlet.ServletException;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import lombok.NonNull;
//import lombok.RequiredArgsConstructor;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
//import org.springframework.stereotype.Component;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//import java.io.IOException;
//import java.nio.file.AccessDeniedException;
//import java.util.Objects;
//
//@Component
//@RequiredArgsConstructor
//public class JwtAuthenticatorFilter extends OncePerRequestFilter {
//
//    private final JwtService jwtService;
//    private final UserDetailsService userDetailsService;
//
//    @Override
//    protected void doFilterInternal(@NonNull HttpServletRequest request,
//                                    @NonNull HttpServletResponse response,
//                                    @NonNull FilterChain filterChain)
//            throws ServletException, IOException, AccessDeniedException {
//        final String authHeader = request.getHeader("Authorization");
//        final String jwt;
//        String userEmail = null;
//        if (Objects.isNull(authHeader) || !authHeader.startsWith("Bearer ")) {
//            filterChain.doFilter(request, response);
////            throw new RuntimeException("Authorization not granted");
//            return;
//        }
//        jwt = authHeader.substring(7);
//        try {
//            userEmail = jwtService.extractUsername(jwt);
//        } catch (Exception e) {
//            handleException(response, e, "An error occurred: " + e.getMessage());
//
//        }
//        if (Objects.nonNull(userEmail) && Objects.isNull(SecurityContextHolder.getContext().getAuthentication())) {
//            UserDetails userDetails = this.userDetailsService.loadUserByUsername(userEmail);
//            if (jwtService.isTokenValid(jwt, userDetails)) {
//                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
//                authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//                SecurityContextHolder.getContext().setAuthentication(authToken);
//            } else {
//                throw new AccessDeniedException("Authorization not granted as token expired");
//            }
//            filterChain.doFilter(request, response);
//        } else {
//            throw new AccessDeniedException("Authorization not granted as user details are not available");
//
//        }
//    }
//    private void handleException(HttpServletResponse response, Exception e, String message) throws IOException {
//        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
//        response.setContentType("application/json");
//        response.getWriter().write("{\"error\": \"" + message + "\"}");
//    }
//}
