package com.newsapp.user.profile.controller;

import com.newsapp.user.profile.model.RegisterRequest;
import com.newsapp.user.profile.model.SaveResponseDTO;
import com.newsapp.user.profile.model.User;
import com.newsapp.user.profile.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/user/")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    @PostMapping("register")
    public ResponseEntity<SaveResponseDTO> register(@RequestBody RegisterRequest request) {
        return ResponseEntity.ok(userService.saveUser(request));
    }

    @GetMapping("email/{email}")
    public Optional<User> findByEmail(@PathVariable("email") String email) {
        return userService.findByEmail(email);
    }
}
