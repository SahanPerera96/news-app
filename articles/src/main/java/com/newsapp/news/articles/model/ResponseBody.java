package com.newsapp.news.articles.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseBody {
    private int statusCode;
    private Status status;
    private String message;
    private Object body;
}
