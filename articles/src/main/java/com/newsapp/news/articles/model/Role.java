package com.newsapp.news.articles.model;

public enum Role {

    USER,
    ADMIN
}
