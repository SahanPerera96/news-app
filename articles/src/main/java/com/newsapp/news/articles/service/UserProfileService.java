package com.newsapp.news.articles.service;

import com.newsapp.news.articles.model.ResponseBody;
import com.newsapp.news.articles.model.Status;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserProfileService {
    private static final Logger logger = LoggerFactory.getLogger(UserProfileService.class);



    public ResponseBody fetchAllUsers() throws Exception {
        try{
            return ResponseBody.builder()
//                    .body(userRepository.findAll())
                    .statusCode(HttpStatus.OK.value())
                    .status(Status.SUCCESS)
                    .build();
        }catch (Exception ex){
            throw new Exception(ex);
        }
    }
}
