package com.newsapp.news.articles.model;

public enum Status {
    SUCCESS,
    FAIL,
    UNAUTHORISED,
    ERROR
}
