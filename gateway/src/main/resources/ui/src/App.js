import logo from "./logo.svg";
import "./App.css";
import React from "react";
import axios from "axios";

function App() {
  React.useEffect(() => {
    fetchDataFromAPI()
  }, [])
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
  
}
const fetchDataFromAPI = async () => {
  let reqBody = {
    "email": "Kallie71@hotmail.com",
    "password": "12345A"
}
    try {
      const response = await axios.post("http://localhost:8222/authenticator/authenticate", reqBody);
      console.log("response: " , response);
    } catch (error) {
      console.log(error);
    }
}
export default App;
