package com.newsapp.api.gateway.model;

public enum Role {

    USER,
    ADMIN
}
