package com.newsapp.api.gateway.feign;

import com.newsapp.api.gateway.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "profile", url = "${application.config.user-profile}")
public interface UserClient {

    @GetMapping("email/{email}")
    Optional<User> findByEmail(@PathVariable("email") String email);
}
