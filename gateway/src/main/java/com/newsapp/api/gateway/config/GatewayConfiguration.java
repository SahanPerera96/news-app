package com.newsapp.api.gateway.config;

import com.newsapp.api.gateway.filters.GatewayAuthCheckFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfiguration {

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder routeLocatorBuilder, GatewayAuthCheckFilter loggingFactory) {
        return routeLocatorBuilder.routes()
                .route("authenticator",
                        route -> route
                                .path("/authenticator/**")
                                .uri("http://localhost:8800"))
                .route("authenticator",
                        route -> route
                                .path("/api/v1/demo/**")
                                .filters(f -> f.filter(loggingFactory.apply(new GatewayAuthCheckFilter.Config())))
                                .uri("http://localhost:8800"))
                .route("profile",
                        route -> route
                                .path("/user/email/**")
                                .filters(f -> f.filter(loggingFactory.apply(new GatewayAuthCheckFilter.Config())))
                                .uri("http://localhost:8001"))
                .build();
    }
}
