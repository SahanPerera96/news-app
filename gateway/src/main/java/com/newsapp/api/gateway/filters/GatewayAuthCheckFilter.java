package com.newsapp.api.gateway.filters;

import com.newsapp.api.gateway.config.JwtService;
import com.newsapp.api.gateway.model.User;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;

import java.util.Objects;

@Component
@AllArgsConstructor
public class GatewayAuthCheckFilter extends AbstractGatewayFilterFactory<GatewayAuthCheckFilter.Config> {

    @Value("${application.config.user-profile}")
    private String USER_URL;
    @Autowired
    JwtService jwtService;
    @Autowired
    RestTemplate template;

    public GatewayAuthCheckFilter() {
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();

            if (!request.getHeaders().containsKey("Authorization")) {
                return this.onError(exchange, "No Authorization header", HttpStatus.UNAUTHORIZED);
            }

            String authorizationHeader = request.getHeaders().get("Authorization").get(0);
            try {
                if (!this.isAuthorizationValid(authorizationHeader)) {
                    return this.onError(exchange, "Invalid Authorization header", HttpStatus.UNAUTHORIZED);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return this.onError(exchange, e.getMessage(), HttpStatus.UNAUTHORIZED);
            }


            return this.onSuccess(exchange, chain);
        };
    }

    private Mono<Void> onError(ServerWebExchange exchange, String err, HttpStatus httpStatus) {
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(httpStatus);

        return response.setComplete();
    }

    public Mono<Void> onSuccess(ServerWebExchange exchange, GatewayFilterChain chain) {
        return chain.filter(exchange);
    }

    private boolean isAuthorizationValid(String authorizationHeader) throws Exception {
        String jwt = authorizationHeader.substring(7);
        String userEmail = jwtService.extractUsername(jwt);
        if (Objects.nonNull(userEmail) && Objects.isNull(SecurityContextHolder.getContext().getAuthentication())) {
            User user = template.getForEntity(USER_URL +"email/"+ userEmail, User.class).getBody();
            if (jwtService.isTokenValid(jwt, user)) {
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authToken);
                if (authToken.isAuthenticated()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static class Config {
        public Config() {
        }
    }
}
